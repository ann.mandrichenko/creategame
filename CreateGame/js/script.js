
function setScrollToAllLinks() {
  $('a[href^="#"], *[data-href^="#"]').on('click', function (e) {
    e.preventDefault();
    const t = 1000;
    const d = $(this).attr('data-href') ? $(this).attr('data-href') : $(this).attr('href');
    if ('#' !== d) {
      $('html,body').stop().animate({
        scrollTop: $(d).offset().top,
      }, t);
    }
  });
}
;
function myMap() {
  const myCenter = new google.maps.LatLng(51.508742, -0.120850);
  const mapCanvas = document.getElementById('map');
  const mapOptions = {
    center: myCenter, zoom: 12,
    styles: [
      {
        elementType: 'geometry',
        stylers: [{
          color: '#242f3e',
        }],
      },
      {
        elementType: 'labels.text.stroke',
        stylers: [{
          color: '#242f3e',
        }],
      },
      {
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#746855',
        }],
      },
      {
        featureType: 'administrative.locality',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#d59563',
        }],
      },
      {
        featureType: 'poi',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#d59563',
        }],
      },
      {
        featureType: 'poi.park',
        elementType: 'geometry',
        stylers: [{
          color: '#263c3f',
        }],
      },
      {
        featureType: 'poi.park',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#6b9a76',
        }],
      },
      {
        featureType: 'road',
        elementType: 'geometry',
        stylers: [{
          color: '#38414e',
        }],
      },
      {
        featureType: 'road',
        elementType: 'geometry.stroke',
        stylers: [{
          color: '#212a37',
        }],
      },
      {
        featureType: 'road',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#9ca5b3',
        }],
      },
      {
        featureType: 'road.highway',
        elementType: 'geometry',
        stylers: [{
          color: '#746855',
        }],
      },
      {
        featureType: 'road.highway',
        elementType: 'geometry.stroke',
        stylers: [{
          color: '#1f2835',
        }],
      },
      {
        featureType: 'road.highway',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#f3d19c',
        }],
      },
      {
        featureType: 'transit',
        elementType: 'geometry',
        stylers: [{
          color: '#2f3948',
        }],
      },
      {
        featureType: 'transit.station',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#d59563',
        }],
      },
      {
        featureType: 'water',
        elementType: 'geometry',
        stylers: [{
          color: '#17263c',
        }],
      },
      {
        featureType: 'water',
        elementType: 'labels.text.fill',
        stylers: [{
          color: '#515c6d',
        }],
      },
      {
        featureType: 'water',
        elementType: 'labels.text.stroke',
        stylers: [{
          color: '#17263c',
        }],
      },
    ],
  };
  const map = new google.maps.Map(mapCanvas, mapOptions);
  const marker = new google.maps.Marker({
    // map: map,
    position: myCenter,
    title: 'Sublime',
    icon: 'images/logo.png',
  });
  marker.setMap(map);
}
;
function firstLetterFontSize(elem, size) {
  const $this = $(elem);
  const words = $this.text().split(' ');
  let newHtml = '';
  for (let i = 0; i < words.length; i++) {
    newHtml += `<span style="font-size:${size}">` +
      `${words[i].substring(0, 1)}</span>${words[i].substring(1)} `;
  }
  $this.html(newHtml);
}
;
function setProjectGallery() {
  filterSelectionProject('all');
  setActiveProject();
}

function setBlogGallery() {
  filterSelectionBlog('all');
  setActiveBlog();
}

function filterSelectionProject(c) {
  filterSelection(c, document.getElementsByClassName('projects')[0]);
}

function filterSelectionBlog(c) {
  filterSelection(c, document.getElementsByClassName('blog')[0]);
}

function filterSelection(c, parent) {
  let x; let i;
  x = parent.getElementsByClassName('pr');
  if (c === 'all') c = '';
  for (i = 0; i < x.length; i++) {
    w3RemoveClass(x[i], 'show');
    w3RemoveClass(x[i].parentElement, 'show_link');
    if (x[i].className.indexOf(c) > -1) {
      w3AddClass(x[i], 'show');
      w3AddClass(x[i].parentElement, 'show_link');
    }
  }
}

function w3AddClass(element, name) {
  let i; let arr1; let arr2;
  arr1 = element.className.split(' ');
  arr2 = name.split(' ');
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) === -1) {
      arr1.push(arr2[i]);
    }
  }
  element.className = arr1.join(' ');
}

function w3RemoveClass(element, name) {
  let i; let arr1; let arr2;
  arr1 = element.className.split(' ');
  arr2 = name.split(' ');
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);
    }
  }
  element.className = arr1.join(' ');
}

function setActiveProject() {
  setActiveBtns('btnContainer');
}

function setActiveBlog() {
  setActiveBtns('blog__btnContainer');
}

function setActiveBtns(parent) {
  const btnContainer = document.getElementById(parent);
  const btns = btnContainer.getElementsByClassName('btn');
  for (let i = 0; i < btns.length; i++) {
    btns[i].addEventListener('click', function () {
      const current = btnContainer.getElementsByClassName('active');
      w3RemoveClass(current[0].className, 'active');
      w3AddClass(this.className,'active');
    });
  }
}
;
function slick(elem) {
  $(elem).slick({
    infinite: true,
    dots: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  });
}
;
